var React = require('react');
var ReactDOM = require('react-dom');
var createReactClass = require('create-react-class');
var axios = require('axios');
var VideoListComponent = require('./videoListComponent.js');
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

var App = createReactClass({
  render:function(){
    return(
      <Router>
        <Switch>
          <Route exact path={"/"} component={HomeComponent}/>
        </Switch>
      </Router>
    );
  }
});

var HomeComponent = createReactClass({
  getInitialState: function(){
    return{
      video:null
    }
    this.handleVideoUpload = this.handleVideoUpload.bind(this);
  },
  render:function(){
    return(
      <div>
      <div className="upload">
      <h1 className="info">Youtube App</h1>
        <form onSubmit={this.handleVideoUpload} >
          <div className="file-upload">
            <input accept="video/*" required onChange={this.handleVideo} id="upload" type="file" name="video"></input><br></br><br></br>
            <input className="login-input" required type="text" ref="title" name="title" placeholder="title"/><br></br>
            <textarea className="login-input" required type="text" ref="description" name="description" placeholder="description"/><br></br>
            <select className="login-input" ref="privacy" name="privacy">
              <option value="private">Private</option>
              <option value="public">Public</option>
              <option value="unlisted">Unlisted</option>
            </select><br></br>
            <button ref="btn" className="signup-button" type="submit" name="button">Upload</button>
          </div>
        </form>
      </div>
      <VideoListComponent/>
      </div>
    );
  },
  handleVideo:function(e){
    this.setState({
      video:e.target.files[0]
    });
  },
  handleVideoUpload:function(e){
    e.preventDefault();
    var self = this;
    self.refs.btn.setAttribute("disabled", "disabled");
    axios({
      method: 'post',
      url: '/videoupload',
      data: {
        title: this.refs.title.value,
        description: this.refs.description.value,
        privacy: this.refs.privacy.value,
        video:this.state.video,
        videoName:this.state.video.name
      },
      config: { headers: {'Content-Type': 'multipart/form-data' }}
      })
      .then(function (response) {
          if(!alert(response.data)){window.location.reload();}
      })
      .catch(function (response) {
          if(!alert(response.data)){window.location.reload();}
    });
  }
});

ReactDOM.render(<App/>,document.getElementById('content'));
