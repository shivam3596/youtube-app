var React = require('react');
var axios = require('axios');
var createReactClass = require('create-react-class');
var VideoComponent = require('./videocomponent.js');

var VideoListComponent = createReactClass({
  getInitialState: function(){
    return{
      videos:null
    }
  },
  componentWillMount(){
    var self = this;
    axios.get('/getvideoslist')
    .then(function (response) {
      self.setState({
        videos:response.data
      });
    })
    .catch(function (error) {
      console.log(error);
    });
  },
  render:function(){
    var videos = this.state.videos;
    if (videos == null) {
        return <div className="info">Loading...</div>
    }
    var video = videos.map(function(item,index){
      return(
          <VideoComponent id={item.contentDetails.videoId} privacy={item.status.privacyStatus} title={item.snippet.title} description={item.snippet.description} thumbnail={item.snippet.thumbnails.medium.url} key={index} />
      );
    }.bind(this));

    return(
      <div>
      <p className="info">All Videos ({videos.length})</p>
        <ul>{video}</ul>
      </div>
    );
  }
});

module.exports = VideoListComponent;
