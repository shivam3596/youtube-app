var React = require('react');
var createReactClass = require('create-react-class');
var EditVideoComponent = require('./editvideocomponent.js');

var VideoComponent = createReactClass({
  render:function(){
    return(
      <li className="liStyle">
        <div className="video">
          <div className="title">
            <h2>{this.props.title}
              <span> : {this.props.privacy}</span>
            </h2>
          </div>
          <div className="thumbnail">
            <img src={this.props.thumbnail}></img>
          </div>
          <div className="description">
            <h3>{this.props.description}</h3>
          </div>
        </div>
        <div className="edit-video">
          <EditVideoComponent videoId={this.props.id} title={this.props.title} privacy={this.props.privacy} description={this.props.description}/>
        </div>
      </li>
    );
  }
});

module.exports = VideoComponent;
