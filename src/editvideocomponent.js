var React = require('react');
var createReactClass = require('create-react-class');
var axios = require('axios');

var EditVideoComponent = createReactClass({
  getInitialState: function(){
    return{
      data:null
    }
  },
  render:function(){
    return(
      <div className="fields">
        <form onSubmit={this.handleVideoUpdate} >
          <div className="file-upload">
            <input type="hidden" ref="id" value={this.props.videoId}></input>
            <input className="login-input" defaultValue={this.props.title} required type="text" ref="title" name="title"/><br></br>
            <textarea className="login-input" defaultValue={this.props.description} required type="text" ref="description" name="description"/><br></br>
            <select className="login-input" ref="privacy" defaultValue={this.props.privacy} name="privacy">
              <option value="private">Private</option>
              <option value="public">Public</option>
              <option value="unlisted">Unlisted</option>
            </select><br></br>
            <button ref="btn" className="signup-button" type="submit" name="button">Update</button>
          </div>
        </form>
      </div>
    );
  },
  handleVideoUpdate:function(e){
    e.preventDefault();
    var self = this;
    self.refs.btn.setAttribute("disabled", "disabled");
    axios({
      method: 'put',
      url: '/videoupdate',
      data: {
        title: this.refs.title.value,
        description: this.refs.description.value,
        privacy: this.refs.privacy.value,
        videoId:this.refs.id.value
      },
      config: { headers: {'Content-Type': 'multipart/form-data' }}
      })
      .then(function (response) {
          if(!alert(response.data)){window.location.reload();}
      })
      .catch(function (response) {
          if(!alert(response.data)){window.location.reload();}
    });
  }
});

module.exports = EditVideoComponent;
