var express = require('express');
var youtubeController = require('./controller/youtubeController.js');
const PORT = process.env.PORT || 5000;
var app = express();
app.set('view engine','ejs');
app.use('/public',express.static('./public'));
var server = app.listen(PORT);
youtubeController(app);
